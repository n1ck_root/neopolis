// gallery
$(document).ready(function() {

	var choosedSlideIndex;
	var imgDescr;
	var imgSrc;
	var modalSlideBlock;
	var galleryMainSlider = $('.gallery__slider');
	var gallerySliderModal = $('.gallery__slider__modal');
	var gallerySlideBlock = galleryMainSlider.find('.gallery__slide-img');
	var gallerySliderAction = $('.gallery__actions__main-slider');
	var galleryModalSliderAction = $('.gallery__actions__modal-slider');
	// var galleryModalTimeline = gsap.timeline({paused: true});
	var activeModalSlide;
	var modalSlideIndex;
	var modalSlideDescr;
	
	// gallery modal slider

	// galleryModalTimeline.fromTo(".gallery__slider__modal__wrap", {visibility: 'hidden', scale: 0, opacity: 0}, {visibility: 'visible', scale: 1, opacity: 1, duration: 0.55});
	
	$('.gallery__slider').on('click', '.gallery__slide-img', function() {
		choosedSlideIndex = +$(this).data('sliderModalIndex');
		$('.gallery__slide').not('.slick-cloned').find('.gallery__slide-img').each(function() {
			imgDescr = $(this).data('photoDescr');
			imgSrc = $(this).find('img').attr('src');
			modalSlideBlock = '<div class="gallery__slider__modal__item" data-descr="'+imgDescr+'"><img src="'+imgSrc+'"></div>'
			$('.gallery__slider__modal').append(modalSlideBlock);
		});
		$('.gallery__slider__modal').on('init', function(event, slick) {
			activeModalSlide = $('.gallery__slider__modal__item.slick-active');
			modalSlideIndex = activeModalSlide.data('slickIndex') + 1;
			modalSlideDescr = activeModalSlide.data('descr');

			$('.gallery__actions__modal-slider__inner').find('#photo_index').text(modalSlideIndex);
			$('.gallery__actions__modal-slider__inner').find('#ptoho_descr').text(modalSlideDescr);
		});	
		$('.gallery__slider__modal').slick({
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: false,
			arrows: true,
			speed: 800,
			nextArrow: '.slider__modal__next',
			prevArrow: '.slider__modal__prev',
			initialSlide: choosedSlideIndex,
			responsive: [
				{
					breakpoint: 991,
					settings: {
						swipe: false
					}
				},
			]
		});
		$('.gallery__slider__modal').on('beforeChange', function(event, slick, currentSlide, nextSlide){
			var nextModalSlide = $('.gallery__slider__modal__item[data-slick-index="'+nextSlide+'"]');
			modalSlideDescr = nextModalSlide.data('descr');
			
			$('.gallery__actions__modal-slider__inner').find('#photo_index').text(nextSlide + 1);
			$('.gallery__actions__modal-slider__inner').find('#ptoho_descr').text(modalSlideDescr);
		});

		gallerySliderAction.hide();
		galleryModalSliderAction.fadeIn(500);
		// galleryModalTimeline.play();
		$('.gallery__slider__modal__wrap').addClass('active');
	});

	$(document).on('click', '.slider__modal__close', function() {
		// galleryModalTimeline.reverse();
		$('.gallery__slider__modal__wrap').removeClass('active');
		galleryModalSliderAction.hide();
		gallerySliderAction.fadeIn(500);
		setTimeout(function() {
			$('.gallery__slider__modal').slick('unslick');
			$('.gallery__slider__modal').empty();
		}, 550);
	});

	// gallery main slider
	galleryMainSlider.on('init', function(event, slick) {
		$('.slick-counter__current').text(slick.currentSlide + 1);
		$('.slick-counter__total').text(slick.slideCount);
		gallerySlideBlock.each(function(i) {
			$(this).attr('data-slider-modal-index', i);
		});
	})
	galleryMainSlider.slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: false,
		arrows: true,
		speed: 1200,
		nextArrow: '.gallery-next',
		prevArrow: '.gallery-prev',
	})
	galleryMainSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
		$('.slick-counter__current').text(nextSlide + 1);
	});

	var modalSliderCloned = $('.gallery__slider__modal__wrap').clone();
	function mobileSlider() {
		if ($(window).width() < 992 && $('.gallery__slider').hasClass('slick-initialized')) {
			galleryMainSlider.slick('unslick');
			$('.gallery__slider__wrap').find('.gallery__slider__modal__wrap').remove();
			$('main').prepend(modalSliderCloned);
			modalSliderCloned.insertBefore($('.section.section-gallery .fp-scrollable'))
		} else if ($(window).width() > 992 && !$('.gallery__slider').hasClass('slick-initialized')) {
			galleryMainSlider.slick({
				infinite: true,
				slidesToShow: 1,
				slidesToScroll: 1,
				dots: false,
				arrows: true,
				speed: 1200,
				nextArrow: '.gallery-next',
				prevArrow: '.gallery-prev',
			})
			$('main').find('.gallery__slider__modal__wrap').remove();
			$('.gallery__slider__wrap').prepend(modalSliderCloned);
		}
	}
	mobileSlider();
	$(window).on('resize', mobileSlider);

	if ($('.content__slider').length) {
		var newsSliderTotal = $('.content__slider__item').length;
		$('.slider-counter__total').text(newsSliderTotal);
		$('.content__slider').on('init', function() {
			$('.slider-counter__current').text(+$('.content__slider__item.slick-active').data('slickIndex') + 1);
		});
		$('.content__slider').slick({
			infinite: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: false,
			arrows: true,
			speed: 1200,
			nextArrow: '.content__photo-next',
			prevArrow: '.content__photo-prev'
		})
		$('.content__slider').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
			$('.slider-counter__current').text(nextSlide + 1);
		});
	}
	
});