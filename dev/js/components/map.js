$(document).ready(function() {
	if ($('.section-location').length) {

		ymaps.ready(['AnimatedLine']).then(init);

		function init (ymaps) {
			var multiRoute = new ymaps.multiRouter.MultiRoute({
					referencePoints: [
						[55.75342532025277,37.61898397265624],
						[44.641376902550164,27.230820930657675]
					],
					params: {
							results: 1
					},
					
			}, {
					// boundsAutoApply: false,
					routeStrokeWidth: 2,
					routeStrokeColor: "#5446FF",
					routeActiveStrokeWidth: 2,
					routeActiveStrokeColor: "#5446FF",
			});
		
			var myMap = new ymaps.Map('map', {
					center: [55.6896635450203,37.44667647600558],
					zoom: 11,
					controls: [],
					// draggable: true
			}, {
				// draggable: true
			});
		
		
		
		//Метки
		
		
			myPlacemark = new ymaps.Placemark([55.746762928810604,37.61896074646566], {
			}, {
				iconLayout: 'default#image',
				iconImageHref: 's/images/useful/svg/circle-way.svg',
				iconImageSize: [110, 116],
				iconImageOffset: [-55, -70],
			});
			
		
			//Линии
		
			var firstAnimatedLine = new ymaps.AnimatedLine([
			[55.746762928810604,37.61896074646566],
			[55.72689225697328,37.58162196444689]
		], {}, {
			strokeColor: "#ED4543",
			strokeWidth: 5,
			animationTime: 1000
		});
			var secondAnimatedLine = new ymaps.AnimatedLine([
			[55.72689225697328,37.58162196444689],
			[55.71029076654499,37.55939424866294]
		], {}, {
			strokeColor: "#ED4543",
			strokeWidth: 5,
			animationTime: 1000
		});
			var thirdAnimatedLine = new ymaps.AnimatedLine([
			[55.71029076654499,37.55939424866294],
			[55.676743010433036,37.50583589905355]
		], {}, {
			strokeColor: "#ED4543",
			strokeWidth: 5,
			animationTime: 1000
		});
			var fourthAnimatedLine = new ymaps.AnimatedLine([
			[55.676743010433036,37.50583589905355],
			[55.66364572603841,37.48300493591878]
		], {}, {
			strokeColor: "#ED4543",
			strokeWidth: 5,
			animationTime: 1000
		});
			var fifthAnimatedLine = new ymaps.AnimatedLine([
			[55.66364572603841,37.48300493591878],
			[55.633019716206704,37.51922548645589]
		], {}, {
			strokeColor: "#ED4543",
			strokeWidth: 5,
			animationTime: 1000
		});
			var sixthAnimatedLine = new ymaps.AnimatedLine([
			[55.633019716206704,37.51922548645589],
			[55.60815328462043,37.48159585644929]
		], {}, {
			strokeColor: "#ED4543",
			strokeWidth: 5,
			animationTime: 1000
		});
			var seventhAnimatedLine = new ymaps.AnimatedLine([
			[55.60815328462043,37.48159585644929],
			[55.59063004563081,37.47551558733368]
		], {}, {
			strokeColor: "#ED4543",
			strokeWidth: 5,
			animationTime: 1000
		});
			var eightAnimatedLine = new ymaps.AnimatedLine([
			[55.59063004563081,37.47551558733368],
			[55.564567752260594,37.44461653948212]
		], {}, {
			strokeColor: "#ED4543",
			strokeWidth: 5,
			animationTime: 1000
		});
		
		// Добавляем линии на карту.
		myMap.geoObjects.add(firstAnimatedLine);
		myMap.geoObjects.add(secondAnimatedLine);
		myMap.geoObjects.add(thirdAnimatedLine);
		myMap.geoObjects.add(fourthAnimatedLine);
		myMap.geoObjects.add(fifthAnimatedLine);
		myMap.geoObjects.add(sixthAnimatedLine);
		myMap.geoObjects.add(seventhAnimatedLine);
		myMap.geoObjects.add(eightAnimatedLine);
		
		// Создаем метки.
		
		
		// HintLayout = ymaps.templateLayoutFactory.createClass( "<div class='my-hint'>" +
		//             "<b>{{ properties.object }}</b><br />" +
		//             "{{ properties.address }}" +
		//             "</div>", {
		//                 // Определяем метод getShape, который
		//                 // будет возвращать размеры макета хинта.
		//                 // Это необходимо для того, чтобы хинт автоматически
		//                 // сдвигал позицию при выходе за пределы карты.
		//                 getShape: function () {
		//                     var el = this.getElement(),
		//                         result = null;
		//                     if (el) {
		//                         var firstChild = el.firstChild;
		//                         result = new ymaps.shape.Rectangle(
		//                             new ymaps.geometry.pixel.Rectangle([
		//                                 [0, 0],
		//                                 [firstChild.offsetWidth, firstChild.offsetHeight]
		//                             ])
		//                         );
		//                     }
		//                     return result;
		//                 }
		//             }
		//         );
		
		
		
		
		
		BalloonContentLayout = ymaps.templateLayoutFactory.createClass(
			'<div>' +
					'<b>{{properties.name}}</b><br />' +
			'</div>', {
		
			// Переопределяем функцию build, чтобы при создании макета начинать
			// слушать событие click на кнопке-счетчике.
			build: function () {
					// Сначала вызываем метод build родительского класса.
					BalloonContentLayout.superclass.build.call(this);
					// А затем выполняем дополнительные действия.
			},
		
		});
		
		var firstPoint = new ymaps.Placemark([55.74276928810604,37.61896074646566], {
			name: 'Метро'
		}, {
			iconLayout: 'default#image',
			iconImageHref: 's/images/useful/svg/m.svg',
			balloonContentLayout: BalloonContentLayout,
		}
		);
		
		var secondPoint = new ymaps.Placemark([55.72689225697328,37.58062196444689], {
			name: 'Метро'
		},{
			iconLayout: 'default#image',
			iconImageHref: 's/images/useful/svg/m.svg',
			balloonContentLayout: BalloonContentLayout,
		});
		
		var thirdPoint = new ymaps.Placemark([55.71029076654499,37.55939424866294], {
			name: 'Метро'
		}, {
			iconLayout: 'default#image',
			iconImageHref: 's/images/useful/svg/m.svg',
			balloonContentLayout: BalloonContentLayout,
		});
		var fourthPoint = new ymaps.Placemark([55.676743010433036,37.50583589905355], {
			name: 'Метро'
		}, {
			iconLayout: 'default#image',
			iconImageHref: 's/images/useful/svg/m.svg',
			balloonContentLayout: BalloonContentLayout,
		});
		var fifthPoint = new ymaps.Placemark([55.66364572603841,37.48300493591878], {
			name: 'Метро'
		}, {
			iconLayout: 'default#image',
			iconImageHref: 's/images/useful/svg/m.svg',
			balloonContentLayout: BalloonContentLayout,
		});
		var sixthPoint = new ymaps.Placemark([55.633019716206704,37.51922548645589], {
			name: 'Метро'
		}, {
			iconLayout: 'default#image',
			iconImageHref: 's/images/useful/svg/m.svg',
			balloonContentLayout: BalloonContentLayout,
		});
		var sevenPoint = new ymaps.Placemark([55.60815328462043,37.48159585644929], {
			name: 'Метро'
		}, {
			iconLayout: 'default#image',
			iconImageHref: 's/images/useful/svg/m.svg',
			balloonContentLayout: BalloonContentLayout,
		});
		var eightPoint = new ymaps.Placemark([55.59063004563081,37.47551558733368], {
			name: 'Маркет'
		}, {
			iconLayout: 'default#image',
			iconImageHref: 's/images/useful/svg/market.svg',
			balloonContentLayout: BalloonContentLayout,
		});
		var ninthPoint = new ymaps.Placemark([55.565567752260594,37.44861653948212], {
			name: 'Метро'
		}, {
			iconLayout: 'default#image',
			iconImageHref: 's/images/useful/svg/m.svg',
			balloonContentLayout: BalloonContentLayout,
		});
		
		// Функция анимации пути.
		function playAnimation() {
		
			secondAnimatedLine.reset();
			thirdAnimatedLine.reset();
			fourthAnimatedLine.reset();
			fifthAnimatedLine.reset();
			sixthAnimatedLine.reset();
			seventhAnimatedLine.reset();
			eightAnimatedLine.reset();
		
			myMap.geoObjects.add(firstPoint);
		
			firstAnimatedLine.animate()
				.then(function() {
					myMap.geoObjects.add(secondPoint)
					return secondAnimatedLine.animate()
				})
				.then(function() {
					return thirdAnimatedLine.animate()
				})
				.then(function() {
					myMap.geoObjects.add(fourthPoint);
					return fourthAnimatedLine.animate()
				})
				.then(function() {
					myMap.geoObjects.add(fifthPoint);
					return fifthAnimatedLine.animate()
				})
				.then(function() {
					myMap.geoObjects.add(sixthPoint);
					return sixthAnimatedLine.animate()
				})
				.then(function() {
					myMap.geoObjects.add(sevenPoint);
					return seventhAnimatedLine.animate()
				})
				.then(function() {
					myMap.geoObjects.add(eightPoint);
					return eightAnimatedLine.animate()
				})
				.then(function() {
					myMap.geoObjects.add(ninthPoint);
				})
		}
			playAnimation();
		
			myMap.controls.add('zoomControl');
			myMap.controls.add('trafficControl');
			myMap.controls.add('geolocationControl');
			myMap.controls.add('rulerControl');
		
			myMap.geoObjects
				.add(myPlacemark)
				.add(multiRoute)
				
		
		
			}

			ymaps.ready(init);
	}
});