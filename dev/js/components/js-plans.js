$(document).ready(function() {
    // plans
    $('.plans__slider-nav').on('click', function() {
        var $this = $(this),
            $slideIndex = $this.data('index');
        if ($this.hasClass('active')) {
            return false
        }
        $this.siblings('.plans__slider-nav.active').removeClass('active');
        $this.addClass('active');
        $('.plans__slider').slick('slickGoTo', $slideIndex);
    });

    // plans slider
    var plansSlider = $('.plans__slider');
    var prevIndex;
    var currentIndex;

    plansSlider.on('init', function() {
        $('.plans__name_mobile').text($('.plans__slider-nav.active').find('span').text());
    });

    plansSlider.slick({
        infinite: true,
        dots: false,
        arrows: true,
        prevArrow: '.plans__arrow.arrow-prev',
        nextArrow: '.plans__arrow.arrow-next',
        speed: 600,
        easing: 'ease-in-out'
    })

    plansSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
        prevIndex = currentSlide;
        currentIndex = nextSlide;
    });

    plansSlider.on('afterChange', function(slick, currentSlide){
        $('.plans__slider-nav.active').removeClass('active');
        $('.plans__slider-nav[data-index="'+currentIndex+'"]').addClass('active');
        $('.plans__name_mobile').text($('.plans__slider-nav.active').find('span').text());
        animateRightSidebar(prevIndex, currentIndex);
    });

    var animateRightSidebar = function(currentIndex, nextIndex) {

        var sidebarTimeline = gsap.timeline({paused: true});

        sidebarTimeline.fromTo($('.js-slider-descr[data-slide="'+currentIndex+'"]').children(), {opacity: 1}, {opacity: 0, duration: 0.4, stagger: 0.03, onComplete: function() {
            $('.js-slider-descr[data-slide="'+currentIndex+'"]').removeClass('active');
            $('.js-slider-descr[data-slide="'+nextIndex+'"]').addClass('active');
        }});
        sidebarTimeline.fromTo($('.js-slider-descr[data-slide="'+nextIndex+'"]').children(), {opacity: 0}, {opacity: 1, duration: 0.4, stagger: 0.03});
        sidebarTimeline.play();
    }
});