$(document).ready(function() {
    //Menu
	var menuTimeline = gsap.timeline({paused: true});
	menuTimeline
	.to('.menu', {zIndex: 100, duration: 0})
	.fromTo(".menu", {visibility: 'hidden', scale: 0.9, opacity: 0}, {visibility: 'visible', scale: 1, opacity: 1, duration: 0.6})
	.fromTo(".menu__list li", {y: 30, opacity: 0}, {y: 0, opacity: 1, duration: 0.2, stagger: 0.1})
	.fromTo(".menu__footer", {y: 20, opacity: 0}, {y: 0, opacity: 1, duration: 0.3, stagger: 0.05});


	$('.js-menu').on('click', function() {
		if ($(this).hasClass('opened')) {
			$(this).removeClass('opened');
			$('.header').removeClass('transparent');
			menuTimeline.reverse().eventCallback("onReverseComplete", function(){ 
				TweenLite.set('.menu',{clearProps:'all'});
			});
		} else {
			$('.header').addClass('transparent');
			$(this).addClass('opened');
			menuTimeline.play();
		}
	});

	$('.menu__list').on('mouseenter', 'li', function() {
		var $this = $(this);
		var $dataImage = $this.data('image');
		if ($this.hasClass('visible')) {
			return false
		}
		$this.siblings('.visible').removeClass('visible');
		$this.addClass('visible');
		$('.menu__container').find('.menu__bg.visible').removeClass('visible')
		$('.menu__container').find('#'+$dataImage).addClass('visible');
	});

	$('.menu__list').on('mouseleave', 'li', function() {
		var $activeItem = $('.menu__list').find('li.active').data('image');
		$('.menu__container').find('.menu__bg.visible').removeClass('visible');
		$('.menu__list').find('li.visible').removeClass('visible');
		$('.menu__container').find('#'+$activeItem).addClass('visible');
	});

	$('.menu__list').on('click', 'li', function() {
		var $this = $(this);
		var $dataImage = $this.data('image');
		if ($this.hasClass('active')) {
			return false
		}
		if ($('.gallery__slider__modal__wrap').hasClass('active')) {
			$('.gallery__slider__modal__wrap').removeClass('active');
			setTimeout(function() {
				$('.gallery__slider__modal').slick('unslick');
				$('.gallery__slider__modal').empty();
			}, 550);
		}
		$this.siblings('.active').removeClass('active');
		$this.addClass('active');
		$('.header').removeClass('transparent');
		$('.menu__container').find('.menu__bg.visible').removeClass('visible')
		$('.menu__container').find('#'+$dataImage).addClass('visible');
		setTimeout(function() {
			$('.js-menu').removeClass('opened');
			menuTimeline.reverse();
		}, 400);
	});
});