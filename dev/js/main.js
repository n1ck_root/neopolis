/* libs */
//=include ./../../node_modules/vivus/dist/vivus.min.js
//=include ./../../node_modules/slick-carousel/slick/slick.min.js
//=include ./../../node_modules/aos/dist/aos.js
//=include lib/modernizr-custom.js
//=include lib/easings.min.js
//=include lib/scrolloverflow.min.js
//=include lib/fullpage.extensions.min.js
//=include lib/fullpage.min.js
//=include lib/line-clamp.min.js
//=include lib/sticky-kit.min.js

/* plugins */

/* separate */
//=include helpers/object-fit.js
//=include separate/global.js

/* components */
//=include components/js-menu.js
//=include components/anim-map.js
//=include components/map.js
//=include components/js-plans.js
//=include components/js-gallery.js

// the main code

var flag = 0;

	

$('#fullpage').fullpage({
	//options here
	scrollHorizontally: true,
	anchors: ['firstBlock', 'about', 'infrastructure', 'location', 'plans', 'gallery', 'news', 'contacts'],
	slidesNavigation: true,
	slidesNavPosition: 'top',
	controlArrows: false,
	dragAndMove: true,
	fixedElements: '.menu, .header',
	menu: '#myMenu',
	normalScrollElements: '.plans__descr-section, .infrastructure__inner-wrap, .gallery__slider__modal__item, .section-location__map, .section-location__info, .section-specifications__items',
	scrollOverflow: true,
	verticalCentered: false,
	autoScrolling: true,
	scrollingSpeed: 900,
	easing: 'easeInOutCubic',
	easingcss3: 'ease',
	fadingEffect: true,
	afterLoad: function(origin, destination, direction) {
	},
	afterRender: function () {
		// setInterval(function () {
		// 		$.fn.fullpage.moveSlideRight();
		// }, 3000);
	},
	afterSlideLoad: function(){

		$('.slide.active [data-aos]').each(function(){
				$(this).addClass("aos-animate");
		});
	},
	onSlideLeave: function(section, origin, destination, direction){
		if (destination.index !== flag) {
			flag = destination.index;
			setTimeout(function() {
				svgPlay();
			}, 300);

			$('.slide [data-aos]').each(function(){
				$(this).removeClass("aos-animate");
			});
		}
	}
});

$('[data-aos]').each(function(){ $(this).addClass("aos-init"); });

	$('.fp-slidesNav ul').wrap('<div class="l-container"></div>');

	function svgPlay() {
		var svgId = $('.slide.active').find('.icon-awward').attr('id');
		new Vivus(svgId, {
			type: 'sync',
			duration: 100,
			delay: 2,
			start: 'autostart'
		});
	}

	AOS.init();

// line clamp
if ($('.section-news__side-list').length) {
	$('.section-news__side-list').each(function(i, item) {
		var item = $(this).find('.news-block__text').get(0);
		$clamp(item, {clamp: 4});
	});
}

// sticky block news open
$(document).ready(function() {
	if ($('.news-content__aside__inner').length) {
		$('.news-content__aside__inner').stick_in_parent({
			sticky_class: 'sticked'
		});
	}
})


//About
var appendFlag = true;
var presentationBtn = $('.section-about__info-presentation').clone();
var awwardDesc = $('.section-about__awward-desc').clone()

$(window).on("load resize scroll",function(e){
	if ($(window).width() < 992) {
		if (appendFlag) {
			$('.section-about__info').find('.section-about__info-presentation').remove();
			$('.section-about__awward').find('.section-about__awward-desc').remove();
			presentationBtn.appendTo('.section-about__parameters');
			awwardDesc.insertAfter('.section-about__info-list');
		}
		appendFlag = false;
 }
 else {
	if (!appendFlag) {
		$('.section-about__parameters').find('.section-about__info-presentation').remove();
		presentationBtn.appendTo('.section-about__info');
		$('.section-about__info-list').find('.section-about__awward-desc').remove();
		awwardDesc.appendTo('.section-about__awward');
	}
	appendFlag = true;
 }
});




// var controller = new ScrollMagic.Controller();

// var tweens = new TimelineMax()
//     .add(TweenMax.to("#section-about__parameters", 1, {transform: "translateY(50)" , opacity: 1 , visibility: 'visible' }),"first")
//     .add(TweenMax.to("#section-about__info-list", 1, {transform: "translateY(20)"}),"first")

// var scene = new ScrollMagic.Scene({
//     triggerElement: '#section-about',
//     triggerHook: "onLeave",
//     duration: "500%"
// })


// .setTween(tweens)
// .setPin('#section-contacts')
// .addIndicators()
// .addTo(controller);




// $(function() {
  

//     var scrollMagicController = new ScrollMagic();
     
//     var title = $('.section-about__parameters'),
//         text = $(elem).find('p'),
//         btn = $(elem).find('a');
    
//     var tl = new TimelineMax({pause: true});
//     tl.add("start") // add timeline label
//       .fromTo(title, 0.4, { y: '40px', opacity: 0 }, { y: 0, opacity: 1, ease: Power2.EaseInOut }, "start")
//       .fromTo(text, 0.4, { y: '60px', opacity: 0 }, { y: 0, opacity: 1, ease: Power2.EaseInOut }, "start")
//       .fromTo(btn, 0.4, { y: '100px', opacity: 0 }, { y: 0, opacity: 1, ease: Power2.EaseInOut }, "start")
    
//     // Create the Scene and trigger when visible
//     var scene = new ScrollScene({
//       triggerElement: '#section-about',
//       offset: 0 /* offset the trigger Npx below scene's top */
//     })
//     .setTween(tl)
//     .addTo(scrollMagicController);

//     // Add debug indicators fixed on right side
//      scene.addIndicators(); 

// });


// var scene = new ScrollMagic.Scene({triggerElement: "#section-about", duration: 300})
// // animate color and top border in relation to scroll position
// .setTween("#section-about__info-list", {borderTop: "30px solid white", backgroundColor: "blue", scale: 0.7}) // the tween durtion can be omitted and defaults to 1
// .addIndicators({name: "2 (duration: 300)"}) // add indicators (requires plugin)
// .addTo(controller);


$('body').addClass('fixed')

$(window).on('load', function() {
  $('.loader').fadeOut().end().delay(400).fadeOut('slow');
});
